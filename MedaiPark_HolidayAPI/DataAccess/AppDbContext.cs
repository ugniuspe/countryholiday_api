﻿using MediaPark_HolidayAPI.Entities;
using MediaPark_HolidayAPI.JsonDeserializers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public  DbSet<DateInCountry> DatesInCountries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Year> Years { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DateInCountry>().HasOne<Year>(d => d.Year).WithMany(y => y.DatesInCountries).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Year>().HasOne<Country>(y => y.Country).WithMany(c => c.Years).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
