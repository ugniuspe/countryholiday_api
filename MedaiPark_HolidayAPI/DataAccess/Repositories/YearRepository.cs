﻿using MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces;
using MediaPark_HolidayAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.DataAccess.Repositories
{
    public class YearRepository : IYearRepository
    {
        private readonly AppDbContext context;

        public YearRepository(AppDbContext context)
        {
            this.context = context;
        }
        public async Task<bool> IsYearAdded(int yearNumber, Country country) {
            var year = await context.Years.Include(y => y.Country).FirstOrDefaultAsync(y => y.Number == yearNumber && y.Country == country);
            return year != null;
        }

        public async Task AddYear(Year year) {
            await context.Years.AddAsync(year);
            await context.SaveChangesAsync();
        }

        public async Task<Year> GetYear(int year, Country country)
        {
            return await context.Years.Include(y => y.Country)
                .FirstOrDefaultAsync(y => y.Country == country && y.Number == year);
        }
        public async Task SetQueryed(Year year) {
            year.Queryed = true;
            context.Years.Update(year);
            await context.SaveChangesAsync();
        }
    }
}
