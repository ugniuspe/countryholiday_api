﻿using MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces;
using MediaPark_HolidayAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.DataAccess.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        private readonly AppDbContext context;
        private readonly int countryCountInApi = 53;

        public CountryRepository(AppDbContext context)
        {
            this.context = context;
        }

        public async Task AddCountries(IEnumerable<Country> countries)
        {
            foreach (var country in countries) {
                if (await context.Countries.FirstOrDefaultAsync(c => c.Name == country.Name) == null) {
                    await context.Countries.AddAsync(country);
                }
            }
            await context.SaveChangesAsync();
        }

        public bool AreCountriesAdded()
        {
            int count = context.Countries.Count();
            if (count == countryCountInApi)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IEnumerable<Country>> GetAllCountries()
        {
            return await context.Countries.ToListAsync();
        }

        public async Task<Country> GetCountryByCode(string countryCode) {
            return await context.Countries.FirstOrDefaultAsync(c => c.CountryCode == countryCode);
        }
    }
}
