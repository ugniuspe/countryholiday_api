﻿using MediaPark_HolidayAPI.Dto;
using MediaPark_HolidayAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces
{
    public interface IDateRepository
    {
        Task AddDate(DateInCountry date);
        Task<DateInCountry> GetDate(Country c, Year year, int month, int day);
        IEnumerable<IEnumerable<DateDto>> GetHolidaysGroupedByMonth(Year year, Country country);
        Task UpdateDay(DateInCountry dateToUpdate, int? dayOfWeek, string name);
        Task<IEnumerable<DateInCountry>> GetHolidaysForYear(Country country, Year year);
    }
}
