﻿using MediaPark_HolidayAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces
{
    public interface ICountryRepository
    {
        Task<IEnumerable<Country>> GetAllCountries();
        bool AreCountriesAdded();
        Task AddCountries(IEnumerable<Country> countries);
        Task<Country> GetCountryByCode(string countryCode);
    }
}
