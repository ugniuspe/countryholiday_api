﻿using MediaPark_HolidayAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces
{
    public interface IYearRepository
    {
        Task<bool> IsYearAdded(int yearNumber, Country country);
        Task AddYear(Year year);
        Task<Year> GetYear(int year, Country country);
        Task SetQueryed(Year year);
        
    }
}
