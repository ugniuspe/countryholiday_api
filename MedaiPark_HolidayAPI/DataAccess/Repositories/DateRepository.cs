﻿using MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces;
using MediaPark_HolidayAPI.Dto;
using MediaPark_HolidayAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.DataAccess.Repositories
{
    public class DateRepository : IDateRepository
    {
        private readonly AppDbContext context;

        public DateRepository(AppDbContext context)
        {
            this.context = context;
        }
        public async Task AddDate(DateInCountry date) {
            await context.DatesInCountries.AddAsync(date);
            await context.SaveChangesAsync();
        }
        public async Task<DateInCountry> GetDate(Country c, Year year, int month, int day) {
            return await context.DatesInCountries.Include(d => d.Year).ThenInclude(y => y.Country)
                .FirstOrDefaultAsync(d => d.Year == year && d.Year.Country == c && d.Month == month && d.Day == day);
        }

        public async Task<IEnumerable<DateInCountry>> GetHolidaysForYear(Country country, Year year)
        {
            return await context.DatesInCountries.Include(d => d.Year).ThenInclude(y => y.Country)
                .Where(d => d.Year.Country == country && d.Year == year).ToListAsync();
        }

        public IEnumerable<IEnumerable<DateDto>> GetHolidaysGroupedByMonth(Year year, Country country) {
            return context.DatesInCountries.Include(d => d.Year).ThenInclude(y => y.Country)
                .Where(d => d.Year == year && d.Year.Country == country).Select(d => new DateDto {
                Country = country.Name,
                DayOfWeek = d.DayOfWeek,
                Year = year.Number,
                Month = d.Month,
                Day = d.Day,
                DayStatus = d.DayStatus.ToString(),
            }).AsEnumerable().GroupBy(d => d.Month);
        }
        public async Task UpdateDay(DateInCountry dateToUpdate, int? dayOfWeek, string name)
        {
            dateToUpdate.DayOfWeek = dayOfWeek;
            dateToUpdate.NameIfHoliday = name;
            context.DatesInCountries.Update(dateToUpdate);
            await context.SaveChangesAsync();
        }
    }
}
