﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Enums
{
    public enum DayStatus
    {
        Holiday,
        FreeDay,
        WorkDay
    }
}
