﻿using MediaPark_HolidayAPI.Dto;
using MediaPark_HolidayAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Controllers
{
    [ApiController]
    [Route("")]
    public class CountryHolidayController : ControllerBase
    {
        private readonly ICountryService countryService;
        private readonly IDateService dateService;

        public CountryHolidayController(ICountryService countryService, IDateService dateService)
        {
            this.countryService = countryService;
            this.dateService = dateService;
        }
        [HttpGet("")]
        public async Task<IEnumerable<CountryDto>> GetAllCountries() {
            return await countryService.GetAllCountries();
        }

        [HttpGet("HolidaysByMonth")]
        public async Task<IActionResult> GetHolidaysByMonth([FromQuery] string countryCode, [FromQuery] int year) {

            var country = await countryService.GetCountry(countryCode);

            if (country != null && country.SupportedFromYear <= year && country.SupportedToYear >= year) {
                return Ok(await dateService.GetHolidaysGroupedByMonth(countryCode, year));
            }

            return BadRequest("Incorrect query");
        }

        [HttpGet("SpecificDayStatus")]
        public async Task<IActionResult> GetDayStatus([FromQuery] string countryCode, [FromQuery] int year, 
            [FromQuery] int month, [FromQuery] int day) {

            var country = await countryService.GetCountry(countryCode);

            if (country != null && country.SupportedFromYear <= year && country.SupportedToYear >= year)
            {
                return Ok(await dateService.GetSpecificDateStatus(countryCode, year, month, day));
            }
            return BadRequest("Incorrect query");
        }

        [HttpGet("MaxFreeDayCount")]
        public async Task<IActionResult> GetMaxFreeDayCount([FromQuery] string countryCode, [FromQuery] int year) {

            var country = await countryService.GetCountry(countryCode);

            if (country != null && country.SupportedFromYear <= year && country.SupportedToYear >= year)
            {
                return Ok(await dateService.GetMaxNumberOfFreeDays(countryCode, year));
            }
            return BadRequest("Incorrect query");
        }
    }
}
