﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Dto
{
    public class CountryDto
    {
        public string Name { get; set; }
    }
}
