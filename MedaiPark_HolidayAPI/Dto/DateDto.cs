﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Dto
{
    public class DateDto
    {
        public string Country { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
#nullable enable
        public string? NameIfHoliday { get; set; }
#nullable disable
        public string DayStatus { get; set; }
        public int? DayOfWeek { get; set; }
    }
}
