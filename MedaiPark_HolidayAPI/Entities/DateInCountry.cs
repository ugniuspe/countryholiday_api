﻿
using MediaPark_HolidayAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Entities
{
    public class DateInCountry
    {
        public int Id { get; set; }
        public int? YearId { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string NameIfHoliday { get; set; }
        public DayStatus DayStatus { get; set; }
        public int? DayOfWeek { get; set; }
        public Year Year { get; set; }
    }
}
