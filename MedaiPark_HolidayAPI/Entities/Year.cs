﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Entities
{
    public class Year
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public bool Queryed { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public IEnumerable<DateInCountry> DatesInCountries { get; set; }
    }
}
