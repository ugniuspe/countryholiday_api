﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Entities
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public int SupportedFromYear { get; set; }
        public int SupportedFromMonth { get; set; }
        public int SupportedFromDay { get; set; }
        public int SupportedToYear { get; set; }
        public int SupportedToMonth { get; set; }
        public int SupportedToDay { get; set; }

        public IEnumerable<Year> Years { get; set; }
    }
}
