﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MediaPark_HolidayAPI.Migrations
{
    public partial class NameIfHoliday : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NameIfHoliday",
                table: "DatesInCountries",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NameIfHoliday",
                table: "DatesInCountries");
        }
    }
}
