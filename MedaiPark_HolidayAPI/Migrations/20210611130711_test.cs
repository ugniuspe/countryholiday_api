﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MediaPark_HolidayAPI.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DatesInCountries_Countries_CountryId",
                table: "DatesInCountries");

            migrationBuilder.DropForeignKey(
                name: "FK_DatesInCountries_Years_YearId",
                table: "DatesInCountries");

            migrationBuilder.DropIndex(
                name: "IX_DatesInCountries_CountryId",
                table: "DatesInCountries");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "DatesInCountries");

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Years",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "YearId",
                table: "DatesInCountries",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Years_CountryId",
                table: "Years",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_DatesInCountries_Years_YearId",
                table: "DatesInCountries",
                column: "YearId",
                principalTable: "Years",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Years_Countries_CountryId",
                table: "Years",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DatesInCountries_Years_YearId",
                table: "DatesInCountries");

            migrationBuilder.DropForeignKey(
                name: "FK_Years_Countries_CountryId",
                table: "Years");

            migrationBuilder.DropIndex(
                name: "IX_Years_CountryId",
                table: "Years");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Years");

            migrationBuilder.AlterColumn<int>(
                name: "YearId",
                table: "DatesInCountries",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "DatesInCountries",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DatesInCountries_CountryId",
                table: "DatesInCountries",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_DatesInCountries_Countries_CountryId",
                table: "DatesInCountries",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DatesInCountries_Years_YearId",
                table: "DatesInCountries",
                column: "YearId",
                principalTable: "Years",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
