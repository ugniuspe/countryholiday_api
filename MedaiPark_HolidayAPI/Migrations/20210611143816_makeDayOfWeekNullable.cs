﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MediaPark_HolidayAPI.Migrations
{
    public partial class makeDayOfWeekNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NameIfHoliday",
                table: "DatesInCountries");

            migrationBuilder.AlterColumn<int>(
                name: "DayOfWeek",
                table: "DatesInCountries",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "DayOfWeek",
                table: "DatesInCountries",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameIfHoliday",
                table: "DatesInCountries",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
