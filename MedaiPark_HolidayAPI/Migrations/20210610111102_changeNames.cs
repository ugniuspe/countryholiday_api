﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MediaPark_HolidayAPI.Migrations
{
    public partial class changeNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SpecificDatesInCountries");

            migrationBuilder.DropTable(
                name: "SupportedCountries");

            migrationBuilder.DropTable(
                name: "YearsinCountries");

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SupportedFromYear = table.Column<int>(type: "int", nullable: false),
                    SupportedFromMonth = table.Column<int>(type: "int", nullable: false),
                    SupportedFromDay = table.Column<int>(type: "int", nullable: false),
                    SupportedToYear = table.Column<int>(type: "int", nullable: false),
                    SupportedToMonth = table.Column<int>(type: "int", nullable: false),
                    SupportedToDay = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Years",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Number = table.Column<int>(type: "int", nullable: false),
                    Queryed = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Years", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DatesInCountries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    YearId = table.Column<int>(type: "int", nullable: false),
                    Month = table.Column<int>(type: "int", nullable: false),
                    Day = table.Column<int>(type: "int", nullable: false),
                    DayStatus = table.Column<int>(type: "int", nullable: false),
                    DayOfWeek = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatesInCountries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DatesInCountries_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DatesInCountries_Years_YearId",
                        column: x => x.YearId,
                        principalTable: "Years",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DatesInCountries_CountryId",
                table: "DatesInCountries",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_DatesInCountries_YearId",
                table: "DatesInCountries",
                column: "YearId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DatesInCountries");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "Years");

            migrationBuilder.CreateTable(
                name: "SupportedCountries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SupportedFromDay = table.Column<int>(type: "int", nullable: false),
                    SupportedFromMonth = table.Column<int>(type: "int", nullable: false),
                    SupportedFromYear = table.Column<int>(type: "int", nullable: false),
                    SupportedToDay = table.Column<int>(type: "int", nullable: false),
                    SupportedToMonth = table.Column<int>(type: "int", nullable: false),
                    SupportedToYear = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportedCountries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "YearsinCountries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Number = table.Column<int>(type: "int", nullable: false),
                    Queryed = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_YearsinCountries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SpecificDatesInCountries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    Day = table.Column<int>(type: "int", nullable: false),
                    DayOfWeek = table.Column<int>(type: "int", nullable: false),
                    DayStatus = table.Column<int>(type: "int", nullable: false),
                    Month = table.Column<int>(type: "int", nullable: false),
                    YearInCountryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecificDatesInCountries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SpecificDatesInCountries_SupportedCountries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "SupportedCountries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SpecificDatesInCountries_YearsinCountries_YearInCountryId",
                        column: x => x.YearInCountryId,
                        principalTable: "YearsinCountries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SpecificDatesInCountries_CountryId",
                table: "SpecificDatesInCountries",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_SpecificDatesInCountries_YearInCountryId",
                table: "SpecificDatesInCountries",
                column: "YearInCountryId");
        }
    }
}
