﻿using MediaPark_HolidayAPI.Dto;
using MediaPark_HolidayAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Services.Interfaces
{
    public interface ICountryService
    {
        public Task<IEnumerable<CountryDto>> GetAllCountries();
        public Task<Country> GetCountry(string countryCode);
    }
}
