﻿using MediaPark_HolidayAPI.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Services.Interfaces
{
    public interface IDateService
    {
        Task<IEnumerable<IEnumerable<DateDto>>> GetHolidaysGroupedByMonth(string countryName, int yearNumber);
        Task<DateStatusDto> GetSpecificDateStatus(string countryCode, int yearNumber, int month, int day);
        Task<int> GetMaxNumberOfFreeDays(string countryCode, int year);
    }
}
