﻿using MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces;
using MediaPark_HolidayAPI.Dto;
using MediaPark_HolidayAPI.Entities;
using MediaPark_HolidayAPI.JsonDeserializers;
using MediaPark_HolidayAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Services
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository repository;

        public CountryService(ICountryRepository repository) {
            this.repository = repository;
        }

        public async Task<Country> GetCountry(string countryCode)
        {
            return await repository.GetCountryByCode(countryCode);            
        }

        public async Task<IEnumerable<CountryDto>> GetAllCountries()
        {
            IEnumerable<Country> countries;
            if (!repository.AreCountriesAdded())
            {
                countries = await EnricoDeserializer.GetAllCountries();
                await repository.AddCountries(countries);
            }
            else {
                countries = await repository.GetAllCountries();
            }
            return countries.Select(c => new CountryDto {
                Name = c.Name,
            });
        }
    }
}
