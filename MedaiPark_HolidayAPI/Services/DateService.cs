﻿using MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces;
using MediaPark_HolidayAPI.Dto;
using MediaPark_HolidayAPI.Entities;
using MediaPark_HolidayAPI.Enums;
using MediaPark_HolidayAPI.JsonDeserializers;
using MediaPark_HolidayAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.Services
{
    public class DateService : IDateService
    {
        private readonly IDateRepository dateRepository;
        private readonly ICountryRepository countryRepository;
        private readonly IYearRepository yearRepository;

        public DateService(IDateRepository dateRepository, ICountryRepository countryRepository, IYearRepository yearRepository)
        {
            this.dateRepository = dateRepository;
            this.countryRepository = countryRepository;
            this.yearRepository = yearRepository;
        }
        public async Task<IEnumerable<IEnumerable<DateDto>>> GetHolidaysGroupedByMonth(string countryCode, int yearNumber) {

            await CheckIfCountriesAdded();

            var country = await countryRepository.GetCountryByCode(countryCode);

            Year year = await GetYear(country, yearNumber);
            IEnumerable<IEnumerable<DateDto>> groupedHolidays = await QueryGroupedDatesOfYear(country, year);

            return groupedHolidays;
        }

        public async Task<int> GetMaxNumberOfFreeDays(string countryCode, int yearNumber)
        {
            await CheckIfCountriesAdded();

            var country = await countryRepository.GetCountryByCode(countryCode);

            Year year = await GetYear(country, yearNumber);


            List<DateInCountry> holidayList = new(await QueryDatesOfYear(country, year));

            return CalculateMaxFreeDayCount(holidayList);
            
        }

        public async Task<DateStatusDto> GetSpecificDateStatus(string countryCode, int yearNumber, int month, int day) {

            await CheckIfCountriesAdded();

            DayStatus status;
            
            var country = await countryRepository.GetCountryByCode(countryCode);

            Year year = await GetYear(country, yearNumber);


            var date = await dateRepository.GetDate(country, year, month, day);
            if (date == null)
            {
                status = await EnricoDeserializer.GetStatus(country, year, month, day);
                await dateRepository.AddDate(new DateInCountry
                {
                    YearId = year.Id,
                    Month = month,
                    Day = day,
                    DayStatus = status,
                    //DateTime max year value = 9999
                    DayOfWeek = year.Number < 10000 ? (int)new DateTime(year.Number, month, day).DayOfWeek : null,
                    NameIfHoliday = null
                });
            }
            else {
                status = date.DayStatus;
            }

            return new DateStatusDto
            {
                Year = year.Number,
                Month = month,
                Day = day,
                Country = country.Name,
                Status = status.ToString(),
            };
        }










        //Checks if countreis are added, if not adds them
        private async Task CheckIfCountriesAdded() {
            if (!countryRepository.AreCountriesAdded())
            {
                var countries = await EnricoDeserializer.GetAllCountries();
                await countryRepository.AddCountries(countries);
            }
        }

        //Checks if year is in DB, if not adds it and returns it
        private async Task<Year> GetYear(Country country, int yearNumber) {
            Year year;
            if (!await yearRepository.IsYearAdded(yearNumber, country))
            {
                year = new Year
                {
                    Number = yearNumber,
                    Queryed = false,
                    CountryId = country.Id,
                };
                await yearRepository.AddYear(year);
            }
            else
            {
                year = await yearRepository.GetYear(yearNumber, country);
            }
            return year;
        }


        //Checks if the year has been queried, if not queries it and returns holidays
        private async Task<IEnumerable<DateInCountry>> QueryDatesOfYear(Country country, Year year) {
            IEnumerable<DateInCountry> holidays;
            if (!year.Queryed)
            {
                holidays = await EnricoDeserializer.GetHolidaysForYear(country, year);

                foreach (var date in holidays)
                {
                    var temp = await dateRepository.GetDate(country, year, date.Month, date.Day);
                    if (temp == null)
                    {
                        await dateRepository.AddDate(date);
                    }
                    else if (temp.DayOfWeek == null || temp.NameIfHoliday == null)
                    {
                        await dateRepository.UpdateDay(temp, date.DayOfWeek, date.NameIfHoliday);
                    }
                    await yearRepository.SetQueryed(year);
                }
            }
            else
            {
                holidays = await dateRepository.GetHolidaysForYear(country, year);
            }

            return holidays;
        }

        //queries for grouped holiday dates if not in db already
        private async Task<IEnumerable<IEnumerable<DateDto>>> QueryGroupedDatesOfYear(Country country, Year year) {
            IEnumerable<IEnumerable<DateDto>> groupedHolidays;
            if (!year.Queryed)
            {
                var holidays = await EnricoDeserializer.GetHolidaysForYear(country, year);

                foreach (var date in holidays)
                {
                    var temp = await dateRepository.GetDate(country, year, date.Month, date.Day);
                    if (temp == null)
                    {
                        await dateRepository.AddDate(date);
                    }
                    else if (temp.DayOfWeek == null || temp.NameIfHoliday == null)
                    {
                        await dateRepository.UpdateDay(temp, date.DayOfWeek, date.NameIfHoliday);
                    }
                }

                groupedHolidays = holidays.Select(h => new DateDto
                {
                    NameIfHoliday = h.NameIfHoliday,
                    Country = country.Name,
                    DayOfWeek = h.DayOfWeek,
                    Year = year.Number,
                    Month = h.Month,
                    Day = h.Day,
                    DayStatus = h.DayStatus.ToString(),
                }).GroupBy(d => d.Month);

                await yearRepository.SetQueryed(year);

            }
            else
            {
                groupedHolidays = dateRepository.GetHolidaysGroupedByMonth(year, country);
            }

            return groupedHolidays;
        }


        //Calcalates the max number of days in year in specific country
        private static int CalculateMaxFreeDayCount(List<DateInCountry> holidayList) {

            int len = holidayList.Count;
            int max = Int32.MinValue;
            int currCount = 0;
            int previousDayOfWeek = Int32.MinValue;

            for (int i = 0; i < len; i++)
            {

                DateInCountry currDay = holidayList[i];
                if (previousDayOfWeek == currDay.DayOfWeek - 1 || currCount == 0)
                {
                    if (currDay.DayOfWeek != 6 && currDay.DayOfWeek != 7)
                    {
                        if (currDay.DayOfWeek == 5)
                        {
                            if (currDay.Day == 30 && currDay.Month == 12)
                            {
                                currCount += 1;
                            }
                            else if (i + 1 < len && holidayList[i + 1].DayOfWeek != 1)
                            {
                                currCount += 2;
                            }
                            previousDayOfWeek = 7;
                        }
                        else if (currDay.DayOfWeek == 1)
                        {
                            if (currDay.Day != 1 && currDay.Month != 1)
                            {
                                currCount += 2;
                            }
                            previousDayOfWeek = 1;
                        }
                        else {
                            previousDayOfWeek = (int) currDay.DayOfWeek;
                        }
                        currCount += 1;
                    }

                    if (currCount > max)
                    {
                        max = currCount;
                    }
                }
                else
                {
                    currCount = 0;
                    previousDayOfWeek = Int32.MinValue;
                }
            }

            return max;
        }
    }
}
