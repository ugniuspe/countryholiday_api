﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.JsonDeserializers.JsonData
{


    public class HolidayJson
    {
        public Date date { get; set; }
        public Name[] name { get; set; }
        public string holidayType { get; set; }
    }

    public class Date
    {
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public int dayOfWeek { get; set; }
    }

    public class Name
    {
        public string lang { get; set; }
        public string text { get; set; }
    }

}
