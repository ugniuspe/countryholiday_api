﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.JsonDeserializers.JsonData
{

    public class IsWorkDay
    {
        public bool isWorkDay { get; set; }
    }

}
