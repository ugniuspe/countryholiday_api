﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.JsonDeserializers.JsonData
{

    public class CountryJson
    {
        public string countryCode { get; set; }
        public string[] regions { get; set; }
        public string[] holidayTypes { get; set; }
        public string fullName { get; set; }
        public Fromdate fromDate { get; set; }
        public Todate toDate { get; set; }
    }

    public class Fromdate
    {
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
    }

    public class Todate
    {
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
    }
}
