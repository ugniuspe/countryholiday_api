﻿using MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces;
using MediaPark_HolidayAPI.Entities;
using MediaPark_HolidayAPI.Enums;
using MediaPark_HolidayAPI.JsonDeserializers.JsonData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.JsonDeserializers
{
    public class EnricoDeserializer
    {
        public async static Task<IEnumerable<Country>> GetAllCountries() {
            HttpClient client = new();
            var response = await client.GetAsync("https://kayaposoft.com/enrico/json/v2.0/?action=getSupportedCountries");
            response.EnsureSuccessStatusCode();

            string responseString = await response.Content.ReadAsStringAsync();
            IEnumerable<CountryJson> countryList = JsonConvert.DeserializeObject<IEnumerable<CountryJson>>(responseString);

            return countryList.Select(c => JsonMapper.MapJsonToCountry(c));
        }

        public async static Task<IEnumerable<DateInCountry>> GetHolidaysForYear(Country country, Year year) {
            HttpClient client = new();
            var response = await client.GetAsync($"https://kayaposoft.com/enrico/json/v2.0/?action=getHolidaysForYear&year={year.Number}&country={country.CountryCode}&holidayType=public_holiday");
            response.EnsureSuccessStatusCode();

            string responseString = await response.Content.ReadAsStringAsync();

            IEnumerable<HolidayJson> holidayList = JsonConvert.DeserializeObject<IEnumerable<HolidayJson>>(responseString);

            return holidayList.Select(h => JsonMapper.MapJsonToDate(h, country, year));
        }

        public async static Task<DayStatus> GetStatus(Country country, Year year, int month, int day) {
            HttpClient client = new();

            HttpResponseMessage response;
            string responseString;
            DayStatus status = DayStatus.FreeDay;

            response = await client.GetAsync($"https://kayaposoft.com/enrico/json/v2.0/?action=isWorkDay&date={day:00}-{month:00}-{year.Number}&country={country.CountryCode}");
            response.EnsureSuccessStatusCode();

            responseString = await response.Content.ReadAsStringAsync();

            bool isWorkDay = JsonConvert.DeserializeObject<IsWorkDay>(responseString).isWorkDay;

            if (isWorkDay) {
                status = DayStatus.WorkDay;
            }

            response = await client.GetAsync($"https://kayaposoft.com/enrico/json/v2.0/?action=isPublicHoliday&date={day:00}-{month:00}-{year.Number}&country={country.CountryCode}");
            response.EnsureSuccessStatusCode();

            responseString = await response.Content.ReadAsStringAsync();

            bool isHoliday = JsonConvert.DeserializeObject<IsPublicHoliday>(responseString).isPublicHoliday;

            if (isHoliday) {
                status = DayStatus.Holiday;
            }

            return status;
        }
    }
}
