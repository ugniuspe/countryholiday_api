﻿using MediaPark_HolidayAPI.DataAccess.Repositories;
using MediaPark_HolidayAPI.DataAccess.Repositories.Interfaces;
using MediaPark_HolidayAPI.Entities;
using MediaPark_HolidayAPI.Enums;
using MediaPark_HolidayAPI.JsonDeserializers.JsonData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaPark_HolidayAPI.JsonDeserializers
{
    public static class JsonMapper
    {
        public static Country MapJsonToCountry(CountryJson c) {
            return new Country()
            {
                Name = c.fullName,
                CountryCode = c.countryCode,
                SupportedFromYear = c.fromDate.year,
                SupportedFromMonth = c.fromDate.month,
                SupportedFromDay = c.fromDate.day,
                SupportedToYear = c.toDate.year,
                SupportedToMonth = c.toDate.month,
                SupportedToDay = c.toDate.day,
            };
        }

        public static DateInCountry MapJsonToDate(HolidayJson h, Country country, Year year) {
            return new DateInCountry
            {
                YearId = year.Id,
                Month = h.date.month,
                Day = h.date.day,
                NameIfHoliday = h.name[0].text,
                DayStatus = DayStatus.Holiday,
                DayOfWeek = h.date.dayOfWeek,
            };
        }
    }
}
